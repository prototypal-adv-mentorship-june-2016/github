import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('repo', { path: 'repo/:owner/:repo' }, function() {
    this.route('issues', { path: 'issues/:issueId' });
  });
  this.authenticatedRoute('dashboard');
});

export default Router;

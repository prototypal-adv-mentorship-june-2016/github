import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';

export default Model.extend({
  login: attr(),
  avatarUrl: attr(),
  type: attr(),
  siteAdmin: attr(),
  name: attr(),
  company: attr(),
  blog: attr(),
  location: attr(),
  email: attr(),
  hireable: attr(),
  bio: attr(),
  publicRepos: attr(),
  publicGists: attr(),
  followers: attr(),
  following: attr(),
  createdAt: attr(),
  updatedAt: attr(),
  repos: hasMany('repo')
});
